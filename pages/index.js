import Head from "next/head";
import styles from "../styles/Home.module.scss";

//components
import Navbar from "../components/Navbar/Navbar";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title> چرم شاپ - charmshop </title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

        <meta
          name="description"
          content="چرم شاپ- کیف چرم،کفش چرم، کت چرم، پالتو چرم، لاکچری، اکسسوری، لباس"
        />
        <link rel="icon" href="/charmfav.svg" />
      </Head>
      <section className="Home_showcase">
        <Navbar />
        <div className="Home_showcase_slider">I'm a slider</div>
      </section>
    </div>
  );
}
