import Link from "next/link";
import styles from "./Navbar.module.scss";
import useMediaQuery from "../../hooks/useMediaQuery";
// let mql = window.matchMedia("(max-width: 800px)");

function Navbar() {
  return (
    <nav className={styles.Navbar}>
      <ul className={styles.Navbar_iconsWrapper}>
        <img src="/icons/cart.svg" />
        <img src="/icons/heart.svg" />
        <img src="/icons/user.svg" />
        <img src="/icons/magnifier.svg" />
      </ul>
      {useMediaQuery(800) ? (
        <img src="/icons/menu.svg" />
      ) : (
        <ul className={styles.Navbar_txtLinksWrapper}>
          <li>
            <Link href="/">
              <a>خانه</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>محصولات</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>درباره ما</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>تماس با ما</a>
            </Link>
          </li>
        </ul>
      )}

      <div>
        <span>CHARMSHOP</span>
      </div>
    </nav>
  );
}
export default Navbar;
